/*
Hexapawn è un gioco tra due giocatori (utente e pc) simile agli scacchi.
La scacchiera è una 3x3.
Ogni giocatore ha 3 pedoni (l'utente ha i pedoni 1, il pc ha i pedoni 2).
Gli spazi vuoti sono indicati da 0.
I pedoni dell'utente sono posizionati inizialmente vicino a lui (riga in basso).
I pedoni del pc sono posizionati inizialmente lontani dall'utente (riga in alto).
Inizialmente la riga centrale è vuota.
A turno ogni giocatore muove un pedone (la prima mossa è dell'utente).
I pedoni possono muoversi di un posto; verso la riga avversaria (solo se il posto di arrivo è vuoto); in diagonale (solo per mangiare un pedone avversario).
Il gioco finisce con la vittoria di un giocatore (non si può pareggiare) e si conclude sempre entro 8 mosse.
Si vince in 3 casi:
	-si eliminano tutti i pedoni avversari;
	-si arriva con un proprio pedone sulla riga avversaria;
	-si impedisce all'avversario di poter compiere alcuna mossa.
La scacchiera è numerata da 1 a 9:

	1	2	3
	4	5	6
	7	8	9

Per indicare una mossa bisogna comunicare due numeri.
Il primo indica la posizione di partenza del pedone.
Il secondo indica la posizione di arrivo del pedone.
L'Intelligenza Artificiale si costruisce per punizione-premio.
Il pc quando deve effettuare una mossa legge lo schema di gioco e visualizza le tutte possibili mosse.
Poi ne sorteggia una randomicamente (con estrazione uniforme sull'elenco di mosse possibili).
Se alla fine del gioco il pc perde, viene eliminata dalla sua memoria l'ultima mossa che ha effettuato.
Se alla fine del gioco il pc vince, vengono aggiunte nella sua memoria tutte le mosse che ha effettuato durante il gioco.
Dopo molti giochi, la memoria del pc conterrà poche mosse finali perdenti, e molte sequenze intere vincenti ripetute.
L'IA imparerà a vincere solo giocando un numero sufficiente di partite.
(Collocare il programma nella stessa cartella dei file di mosse. Avviare il programma in ROOT.)
*/

#include <stdlib.h>







{
	TRandom3 generami(0);										//generatore di numeri pseudo-casuali
	vector<int> schema;											//vettore per creare la numerazione delle posizioni
	vector<int> table;											//vettore per creare lo schema della scacchiera
	for(int i=0;i<9;i++){										//ciclo per creare la numerazione delle posizioni
		schema.push_back(i+1);
	}
	for(int i=0;i<9;i++){										//ciclo per creare la scacchiera vuota
		table.push_back(0);
	}
	cout<<endl<<"NUMERAZIONE DELLE POSIZIONI"<<endl;
	cout<<"	computer"<<endl;
	for(int j=0;j<7;j++){										//stampo a schermo la numerazione delle posizioni
		for(int k=0;k<3;k++){
			cout<<schema[j+k]<<"	";
		}
		j=j+2;
		cout<<endl;
	}
	cout<<"	utente"<<endl<<endl;
	cout<<"SCHEMA INIZIALE"<<endl<<"	computer"<<endl;
	int n;
	int k=0;
	for(int i=0;i<3;i++){										//stampo a schermo la scacchiera con i pedoni nelle posizioni iniziali
		if(i==0) n=2;
		if(i==1) n=0;
		if(i==2) n=1;
		for(int j=0;j<3;j++){
			cout<<n<<"	";
			table[k]=n;
			k++;
		}
		cout<<endl;
	}
	cout<<"	utente"<<endl<<endl;
	int start;													//posizione di partenza di una mossa dell'utente
	int finish;													//posizione di arrivo di una mossa dell'utente
	int sopra;													//posizione sopra start
	int left;													//posizione sopra e a sx di start
	int right;													//posizione sopra e a dx di start
	int counterof2;												//conta i pedoni del pc
	int winchance;												//conta le possibilità di aver vinto nella struttura di controllo della mossa dell'utente
	int numschema;												//numero che indetifica lo schema della scacchiera
	int o;
	int a;
	int v;
	int posizione;												//indice di un vettore
	int pcstart;												//posizione iniziale della mossa del pc
	int pcfinish;												//posizione finale della mossa del pc
	vector<int> mosse;											//vettore delle mosse possibili del pc (numeri a 2 cifre; la prima cifra indica la posizione di partenza; la seconda cifra indica la posizione di arrivo)
	vector<int> numfile;										//vettore dei codici identificativi dei file mosse aperti
	vector<int> codmossa;										//vettore delle mosse utilizzate dal pc
	vector<int> contenuto;										//vettore delle mosse possibili del pc con ripetuta la mossa effettuata
	int counterof1;												//conta i pedoni dell'utente
	int losechance;												//conta le possibilità di aver perso nella struttura di controllo della mossa del pc
	int win=0;													//variabile per controllare la vincita dell'utente e uscire dal gioco
	int lose=0;													//variabile per controllare la vincita del pc e uscire dal gioco
	int exit;													//variabile per uscire da un ciclo
	while(win==0&&lose==0){										//ciclo di un turno di utente e poi di pc con strutture di controllo
		cout<<"Dimmi quale mossa vuoi fare (il primo numero indica la posizione di partenza il secondo quella di arrivo del pedone)"<<endl;
		cin>>start;
		cin>>finish;
		while(table[start-1]!=1){								//ciclo per controllare che una mossa dell'utente sia legittima (partenza)
			cout<<"Non hai pedoni nel punto di partenza"<<endl;
			cout<<"Dimmi di nuovo partenza e arrivo"<<endl;
			cin>>start;
			cin>>finish;
		}
		exit=0;
		while(exit==0){											//ciclo per controllare che una mossa dell'utente sia legittima (confini della scacchiera e regole del gioco)
			sopra=start-4;
			left=sopra-1;
			right=sopra+1;
			if((table[start-1]==1)&&(((finish-1)==sopra&&table[sopra]==0)||((finish-1)==left&&table[left]==2&&sopra!=0&&sopra!=3&&sopra!=6)||((finish-1)==right&&table[right]==2&&sopra!=2&&sopra!=5&&sopra!=8))){
				exit=1;
			}
			else{
				cout<<"La mossa non è consentita"<<endl;
				cout<<"Dimmi di nuovo partenza e arrivo"<<endl;
				cin>>start;
				cin>>finish;
			}
		}
		cout<<endl;
		table[start-1]=0;										//svuoto la posizione di partenza
		table[finish-1]=1;										//metto un pedone dell'utente in posizione di arrivo
		cout<<"	computer"<<endl;
		for(int j=0;j<7;j++){									//stampo a schermo la scacchiera attuale (dopo la mossa dell'utente)
			for(int k=0;k<3;k++){
				cout<<table[j+k]<<"	";
			}
			j=j+2;
			cout<<endl;
		}
		cout<<"	UTENTE"<<endl<<endl;
		system("read -n 1 -s -p \"Premi un tasto per continuare...\"");
		cout<<endl<<endl;
		if(table[0]==1||table[1]==1||table[2]==1){				//se un pedone dell'utente arriva dall'altra parte della scacchiera (vittoria)
			cout<<"Hai vinto!!!"<<endl<<endl;
			win=1;
		}
		else{													//se nessun pedone dell'utente arriva dall'altra parte della scacchiera
			counterof2=0;
			winchance=0;
			for(int i=0;i<9;i++){
				if(table[i]==2){
					counterof2++;								//conta i pedoni del pc sulla scacchiera
					if(i!=6&&i!=7&&i!=8){						//se il pedone del pc non è dal lato dell'utente...
						if(table[i+3]!=0){						//... e quel pedone del pc non può scendere in basso
							winchance++;						//è una possibilità in più per vincere
						}
					}
					if(i==2||i==5||i==6||i==7||i==8){
						winchance++;
					}
					if(i!=2&&i!=5&&i!=6&&i!=7&&i!=8){			//se il pedone del pc si trova in 1 o 2 o 4 o 5...
						if(table[i+4]!=1){						//... e sotto a dx non vi è un pedone dell'utente (non può mangiare)
							winchance++;						//è una possibilità in più per vincere
						}
					}
					if(i==0||i==3||i==6||i==7||i==8){
						winchance++;
					}
					if(i!=0&&i!=3&&i!=6&&i!=7&&i!=8){			//se il pedone avversario si trova in 2 o 3 o 5 o 6...
						if(table[i+2]!=1){						//... e sotto a sx non vi è un pedone dell'utente (non può mangiare)
							winchance++;						//è una possibilità in più per vincere
						}
					}
				}
			}
			if(winchance==3*counterof2||counterof2==0){			//se per ogni pedone del pc presente si hanno 3 winchance (quel pedone non può scendere né in basso, né in basso a sx, né in basso a dx) e se non ci sono più pedoni del pc (vittoria)
				cout<<"Hai vinto!!!"<<endl<<endl;
				win=1;
			}
		}
		if(win==0){												//se l'utente non ha vinto
			numschema=0;
			o=8;
			for(int i=0;i<9;i++){								//ciclo che converte la scacchiera attuale in numschema
				numschema=numschema+table[i]*pow(10,o);
				o--;
			}
			ifstream leggimi;									//apertura del file di mosse identificato da numschema
			if(numschema==2012000){
				leggimi.open("002012000.txt");
			}
			if(numschema==2111000){
				leggimi.open("002111000.txt");
			}
			if(numschema==2122000){
				leggimi.open("002122000.txt");
			}
			if(numschema==2210000){
				leggimi.open("002210000.txt");
			}
			if(numschema==2221000){
				leggimi.open("002221000.txt");
			}
			if(numschema==20021000){
				leggimi.open("020021000.txt");
			}
			if(numschema==20112000){
				leggimi.open("020112000.txt");
			}
			if(numschema==20120000){
				leggimi.open("020120000.txt");
			}
			if(numschema==20211000){
				leggimi.open("020211000.txt");
			}
			if(numschema==22010001){
				leggimi.open("022010001.txt");
			}
			if(numschema==22010100){
				leggimi.open("022010100.txt");
			}
			if(numschema==22021100){
				leggimi.open("022021100.txt");
			}
			if(numschema==22101100){
				leggimi.open("022101100.txt");
			}
			if(numschema==22120001){
				leggimi.open("022120001.txt");
			}
			if(numschema==22211100){
				leggimi.open("022211100.txt");
			}
			if(numschema==200012000){
				leggimi.open("200012000.txt");
			}
			if(numschema==200111000){
				leggimi.open("200111000.txt");
			}
			if(numschema==200122000){
				leggimi.open("200122000.txt");
			}
			if(numschema==200210000){
				leggimi.open("200210000.txt");
			}
			if(numschema==200221000){
				leggimi.open("200221000.txt");
			}
			if(numschema==202001100){
				leggimi.open("202001100.txt");
			}
			if(numschema==202011010){
				leggimi.open("202011010.txt");
			}
			if(numschema==202012100){
				leggimi.open("202012100.txt");
			}
			if(numschema==202100001){
				leggimi.open("202100001.txt");
			}
			if(numschema==202102010){
				leggimi.open("202102010.txt");
			}
			if(numschema==202110010){
				leggimi.open("202110010.txt");
			}
			if(numschema==202201010){
				leggimi.open("202201010.txt");
			}
			if(numschema==202210001){
				leggimi.open("202210001.txt");
			}
			if(numschema==220010001){
				leggimi.open("220010001.txt");
			}
			if(numschema==220010100){
				leggimi.open("220010100.txt");
			}
			if(numschema==220021100){
				leggimi.open("220021100.txt");
			}
			if(numschema==220101001){
				leggimi.open("220101001.txt");
			}
			if(numschema==220112001){
				leggimi.open("220112001.txt");
			}
			if(numschema==220120001){
				leggimi.open("220120001.txt");
			}
			if(numschema==222001110){
				leggimi.open("222001110.txt");
			}
			if(numschema==222010101){
				leggimi.open("222010101.txt");
			}
			if(numschema==222100011){
				leggimi.open("222100011.txt");
			}
			numfile.push_back(numschema);						//riempimento del vettore dei codici file mosse aperti
			mosse.clear();										//svuotamento del vettore di mosse
			while(leggimi>>a){									//lettura del file mosse e riempimento del vettore mosse del pc
				mosse.push_back(a);
			}
			leggimi.close();
			if(mosse.size()==0){								//se lo schema attuale non contiene mosse possibili per il pc l'utente ha vinto
				cout<<"Hai vinto!!!"<<endl<<endl;
				win=1;
			}
			else{												//se il pc può effettuare mosse
				posizione=generami.Integer(mosse.size());		//indice del vettore mosse
				codmossa.push_back(mosse.at(posizione));		//aggiunta della mossa mosse.at(posizione) al vettore codmossa
				pcstart=mosse.at(posizione)/10;					//conversione del codice mossa nella posizione inziale
				pcfinish=mosse.at(posizione)-10*pcstart;		//conversione del codice mossa nella posizione finale
				table.at(pcstart-1)=0;							//svuotamento della posizione inziale
				table.at(pcfinish-1)=2;							//riempimento della posizione finale con un pedone del pc
				cout<<"	COMPUTER								(NUMERAZIONE POSIZIONI)"<<endl;
				for(int j=0;j<7;j++){							//stampo a schermo lo schema attuale della scacchiera e la numerazione delle posizioni
					for(int k=0;k<3;k++){
						cout<<table[j+k]<<"	";
					}
					cout<<"							|";
					for(int k=0;k<3;k++){
						cout<<schema[j+k]<<"	";
					}
					j=j+2;
					cout<<endl;
				}
				cout<<"	utente"<<endl<<endl;
				system("read -n 1 -s -p \"Premi un tasto per continuare...\"");
				cout<<endl<<endl;
				if(table[6]==2||table[7]==2||table[8]==2){		//se un pedone del pc arriva dal lato dell'utente (sconfitta dell'utente)
				cout<<"Hai perso!!!"<<endl<<endl;
				lose=1;
				}
				else{											//se nessun pedone del pc arriva dal lato dell'utente
					counterof1=0;
					losechance=0;
					for(int i=0;i<9;i++){
						if(table[i]==1){
							counterof1++;						//conta i pedoni dell'utente sulla scacchiera
							if(i!=0&&i!=1&&i!=2){				//se il pedone dell'utente non è dal lato del pc...
								if(table[i-3]!=0){				//... e quel pedone del pc non può salire in alto
									losechance++;				//è una possibilità in più per perdere
								}
							}
							if(i==0||i==1||i==2||i==3||i==6){
								losechance++;
							}
							if(i!=0&&i!=1&&i!=2&&i!=3&&i!=6){	//se il pedone dell'utente si trova in 5 o 6 o 8 o 9...
								if(table[i-4]!=2){				//... e sopra a sx non vi è un pedone del pc (non può mangiare)
									losechance++;				//è una possibilità in più per perdere
								}
							}
							if(i==0||i==1||i==2||i==5||i==8){
								losechance++;
							}
							if(i!=0&&i!=1&&i!=2&&i!=5&&i!=8){	//se il pedone dell'utente si trova in 4 o 5 o 7 o 8...
								if(table[i-2]!=2){				//... e sopra a sx non vi è un pedone del pc (non può mangiare)
									losechance++;				//è una possibilità in più per perdere
								}
							}
						}
					}
					if(losechance==3*counterof1||counterof1==0){//se per ogni pedone dell'utente presente si hanno 3 losechance (quel pedone non può salire né in alto, né in alto a sx, né in alto a dx) e se non ci sono più pedoni dell'utente (sconfitta dell'utente)
					cout<<"Hai perso!!!"<<endl<<endl;
					lose=1;
					}
				}
			}
		}
		if(win==1){												//in caso di vittoria dell'utente
			mosse.erase(mosse.begin()+posizione);				//si elimina l'ultima mossa utilizzata dall'ultimo vettore delle mosse del pc
			ofstream scrivimi;									//si scrive il file dello schema dell'ultima mossa fatta dal pc
			if(numschema==2012000){
				scrivimi.open("002012000.txt");
			}
			if(numschema==2111000){
				scrivimi.open("002111000.txt");
			}
			if(numschema==2122000){
				scrivimi.open("002122000.txt");
			}
			if(numschema==2210000){
				scrivimi.open("002210000.txt");
			}
			if(numschema==2221000){
				scrivimi.open("002221000.txt");
			}
			if(numschema==20021000){
				scrivimi.open("020021000.txt");
			}
			if(numschema==20112000){
				scrivimi.open("020112000.txt");
			}
			if(numschema==20120000){
				scrivimi.open("020120000.txt");
			}
			if(numschema==20211000){
				scrivimi.open("020211000.txt");
			}
			if(numschema==22010001){
				scrivimi.open("022010001.txt");
			}
			if(numschema==22010100){
				scrivimi.open("022010100.txt");
			}
			if(numschema==22021100){
				scrivimi.open("022021100.txt");
			}
			if(numschema==22101100){
				scrivimi.open("022101100.txt");
			}
			if(numschema==22120001){
				scrivimi.open("022120001.txt");
			}
			if(numschema==22211100){
				scrivimi.open("022211100.txt");
			}
			if(numschema==200012000){
				scrivimi.open("200012000.txt");
			}
			if(numschema==200111000){
				scrivimi.open("200111000.txt");
			}
			if(numschema==200122000){
				scrivimi.open("200122000.txt");
			}
			if(numschema==200210000){
				scrivimi.open("200210000.txt");
			}
			if(numschema==200221000){
				scrivimi.open("200221000.txt");
			}
			if(numschema==202001100){
				scrivimi.open("202001100.txt");
			}
			if(numschema==202011010){
				scrivimi.open("202011010.txt");
			}
			if(numschema==202012100){
				scrivimi.open("202012100.txt");
			}
			if(numschema==202100001){
				scrivimi.open("202100001.txt");
			}
			if(numschema==202102010){
				scrivimi.open("202102010.txt");
			}
			if(numschema==202110010){
				scrivimi.open("202110010.txt");
			}
			if(numschema==202201010){
				scrivimi.open("202201010.txt");
			}
			if(numschema==202210001){
				scrivimi.open("202210001.txt");
			}
			if(numschema==220010001){
				scrivimi.open("220010001.txt");
			}
			if(numschema==220010100){
				scrivimi.open("220010100.txt");
			}
			if(numschema==220021100){
				scrivimi.open("220021100.txt");
			}
			if(numschema==220101001){
				scrivimi.open("220101001.txt");
			}
			if(numschema==220112001){
				scrivimi.open("220112001.txt");
			}
			if(numschema==220120001){
				scrivimi.open("220120001.txt");
			}
			if(numschema==222001110){
				scrivimi.open("222001110.txt");
			}
			if(numschema==222010101){
				scrivimi.open("222010101.txt");
			}
			if(numschema==222100011){
				scrivimi.open("222100011.txt");
			}
			for(int h=0;h<mosse.size();h++){					//si scrive nel file il vettore delle mosse possibili (a cui è stata tolta la mossa effettuata dal pc)
				scrivimi<<mosse.at(h)<<endl;
			}
			scrivimi.close();
		}
		if(lose==1){											//in caso di vittoria del pc
			for(int g=0;g<numfile.size();g++){
				ifstream lettura;								//si apre uno dei file aperti dal pc per scegliere la mossa da fare
				if(numfile.at(g)==2012000){
					lettura.open("002012000.txt");
				}
				if(numfile.at(g)==2111000){
					lettura.open("002111000.txt");
				}
				if(numfile.at(g)==2122000){
					lettura.open("002122000.txt");
				}
				if(numfile.at(g)==2210000){
					lettura.open("002210000.txt");
				}
				if(numfile.at(g)==2221000){
					lettura.open("002221000.txt");
				}
				if(numfile.at(g)==20021000){
					lettura.open("020021000.txt");
				}
				if(numfile.at(g)==20112000){
					lettura.open("020112000.txt");
				}
				if(numfile.at(g)==20120000){
					lettura.open("020120000.txt");
				}
				if(numfile.at(g)==20211000){
					lettura.open("020211000.txt");
				}
				if(numfile.at(g)==22010001){
					lettura.open("022010001.txt");
				}
				if(numfile.at(g)==22010100){
					lettura.open("022010100.txt");
				}
				if(numfile.at(g)==22021100){
					lettura.open("022021100.txt");
				}
				if(numfile.at(g)==22101100){
					lettura.open("022101100.txt");
				}
				if(numfile.at(g)==22120001){
					lettura.open("022120001.txt");
				}
				if(numfile.at(g)==22211100){
					lettura.open("022211100.txt");
				}
				if(numfile.at(g)==200012000){
					lettura.open("200012000.txt");
				}
				if(numfile.at(g)==200111000){
					lettura.open("200111000.txt");
				}
				if(numfile.at(g)==200122000){
					lettura.open("200122000.txt");
				}
				if(numfile.at(g)==200210000){
					lettura.open("200210000.txt");
				}
				if(numfile.at(g)==200221000){
					lettura.open("200221000.txt");
				}
				if(numfile.at(g)==202001100){
					lettura.open("202001100.txt");
				}
				if(numfile.at(g)==202011010){
					lettura.open("202011010.txt");
				}
				if(numfile.at(g)==202012100){
					lettura.open("202012100.txt");
				}
				if(numfile.at(g)==202100001){
					lettura.open("202100001.txt");
				}
				if(numfile.at(g)==202102010){
					lettura.open("202102010.txt");
				}
				if(numfile.at(g)==202110010){
					lettura.open("202110010.txt");
				}
				if(numfile.at(g)==202201010){
					lettura.open("202201010.txt");
				}
				if(numfile.at(g)==202210001){
					lettura.open("202210001.txt");
				}
				if(numfile.at(g)==220010001){
					lettura.open("220010001.txt");
				}
				if(numfile.at(g)==220010100){
					lettura.open("220010100.txt");
				}
				if(numfile.at(g)==220021100){
					lettura.open("220021100.txt");
				}
				if(numfile.at(g)==220101001){
					lettura.open("220101001.txt");
				}
				if(numfile.at(g)==220112001){
					lettura.open("220112001.txt");
				}
				if(numfile.at(g)==220120001){
					lettura.open("220120001.txt");
				}
				if(numfile.at(g)==222001110){
					lettura.open("222001110.txt");
				}
				if(numfile.at(g)==222010101){
					lettura.open("222010101.txt");
				}
				contenuto.clear();
				while(lettura>>v){								//si legge il contenuto del file e si colloca nel vettore contenuto
					contenuto.push_back(v);
				}
				lettura.close();
				contenuto.push_back(codmossa.at(g));			//in fondo al vettore contenuto si ripete la mossa effettuata dal pc
				ofstream scrittura;								//si riscrive il file appena aperto
				if(numfile.at(g)==2012000){
					scrittura.open("002012000.txt");
				}
				if(numfile.at(g)==2111000){
					scrittura.open("002111000.txt");
				}
				if(numfile.at(g)==2122000){
					scrittura.open("002122000.txt");
				}
				if(numfile.at(g)==2210000){
					scrittura.open("002210000.txt");
				}
				if(numfile.at(g)==2221000){
					scrittura.open("002221000.txt");
				}
				if(numfile.at(g)==20021000){
					scrittura.open("020021000.txt");
				}
				if(numfile.at(g)==20112000){
					scrittura.open("020112000.txt");
				}
				if(numfile.at(g)==20120000){
					scrittura.open("020120000.txt");
				}
				if(numfile.at(g)==20211000){
					scrittura.open("020211000.txt");
				}
				if(numfile.at(g)==22010001){
					scrittura.open("022010001.txt");
				}
				if(numfile.at(g)==22010100){
					scrittura.open("022010100.txt");
				}
				if(numfile.at(g)==22021100){
					scrittura.open("022021100.txt");
				}
				if(numfile.at(g)==22101100){
					scrittura.open("022101100.txt");
				}
				if(numfile.at(g)==22120001){
					scrittura.open("022120001.txt");
				}
				if(numfile.at(g)==22211100){
					scrittura.open("022211100.txt");
				}
				if(numfile.at(g)==200012000){
					scrittura.open("200012000.txt");
				}
				if(numfile.at(g)==200111000){
					scrittura.open("200111000.txt");
				}
				if(numfile.at(g)==200122000){
					scrittura.open("200122000.txt");
				}
				if(numfile.at(g)==200210000){
					scrittura.open("200210000.txt");
				}
				if(numfile.at(g)==200221000){
					scrittura.open("200221000.txt");
				}
				if(numfile.at(g)==202001100){
					scrittura.open("202001100.txt");
				}
				if(numfile.at(g)==202011010){
					scrittura.open("202011010.txt");
				}
				if(numfile.at(g)==202012100){
					scrittura.open("202012100.txt");
				}
				if(numfile.at(g)==202100001){
					scrittura.open("202100001.txt");
				}
				if(numfile.at(g)==202102010){
					scrittura.open("202102010.txt");
				}
				if(numfile.at(g)==202110010){
					scrittura.open("202110010.txt");
				}
				if(numfile.at(g)==202201010){
					scrittura.open("202201010.txt");
				}
				if(numfile.at(g)==202210001){
					scrittura.open("202210001.txt");
				}
				if(numfile.at(g)==220010001){
					scrittura.open("220010001.txt");
				}
				if(numfile.at(g)==220010100){
					scrittura.open("220010100.txt");
				}
				if(numfile.at(g)==220021100){
					scrittura.open("220021100.txt");
				}
				if(numfile.at(g)==220101001){
					scrittura.open("220101001.txt");
				}
				if(numfile.at(g)==220112001){
					scrittura.open("220112001.txt");
				}
				if(numfile.at(g)==220120001){
					scrittura.open("220120001.txt");
				}
				if(numfile.at(g)==222001110){
					scrittura.open("222001110.txt");
				}
				if(numfile.at(g)==222010101){
					scrittura.open("222010101.txt");
				}
				for(int x=0;x<contenuto.size();x++){			//si scrive nel file il contenuto del vettore contenuto (si riscrive il file aggiungendo in coda la mossa effettuata dal pc)
					scrittura<<contenuto.at(x)<<endl;
				}
				scrittura.close();
			}
		}
	}
}
