# Hexapawn



## Come giocare

Hexapawn è un gioco tra due giocatori (utente e pc) simile agli scacchi.
La scacchiera è una 3x3.
Ogni giocatore ha 3 pedoni (l'utente ha i pedoni 1, il pc ha i pedoni 2).
Gli spazi vuoti sono indicati da 0.
I pedoni dell'utente sono posizionati inizialmente vicino a lui (riga in basso).
I pedoni del pc sono posizionati inizialmente lontani dall'utente (riga in alto).
Inizialmente la riga centrale è vuota.
A turno ogni giocatore muove un pedone (la prima mossa è dell'utente).
I pedoni possono muoversi di un posto; verso la riga avversaria (solo se il posto di arrivo è vuoto); in diagonale (solo per mangiare un pedone avversario).
Il gioco finisce con la vittoria di un giocatore (non si può pareggiare) e si conclude sempre entro 8 mosse.

## Come vincere

Si vince in 3 casi:
	-si eliminano tutti i pedoni avversari;
	-si arriva con un proprio pedone sulla riga avversaria;
	-si impedisce all'avversario di poter compiere alcuna mossa.

## Come muovere i pedoni

La scacchiera è numerata da 1 a 9:

	1	2	3
	4	5	6
	7	8	9

Per indicare una mossa bisogna comunicare due numeri.
Il primo indica la posizione di partenza del pedone.
Il secondo indica la posizione di arrivo del pedone.

## Come funziona L'IA

L'Intelligenza Artificiale si costruisce per punizione-premio.
Il pc quando deve effettuare una mossa legge lo schema di gioco e visualizza le tutte possibili mosse.
Poi ne sorteggia una randomicamente (con estrazione uniforme sull'elenco di mosse possibili).
Se alla fine del gioco il pc perde, viene eliminata dalla sua memoria l'ultima mossa che ha effettuato.
Se alla fine del gioco il pc vince, vengono aggiunte nella sua memoria tutte le mosse che ha effettuato durante il gioco.
Dopo molti giochi, la memoria del pc conterrà poche mosse finali perdenti, e molte sequenze intere vincenti ripetute.
L'IA imparerà a vincere solo giocando un numero sufficiente di partite.

## Come usare Hexapawn

Fare il download di hexapawn.cpp e della cartella contenente il set iniziale di mosse.
Collocare il programma nella stessa cartella dei file di mosse.
Avviare il programma in ROOT


